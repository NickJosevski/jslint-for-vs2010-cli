﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JSLint.VS2010.OptionClasses;
using System.IO;

namespace JSLint.VS2010.test
{
    [TestClass]
    public class SettingsTest
    {
        public Options LoadOptionsFromEmbeddedResource(string name)
        {
            using (Stream s = typeof(SettingsTest).Assembly.GetManifestResourceStream("JSLint.VS2010.test." + name))
            {
                return Options.Load(s);
            }
        }

        [TestMethod]
        public void TestLoadingAllErrors1_2_4()
        {
            Options options = LoadOptionsFromEmbeddedResource("SettingFiles._1_2_4.ALLErrors.xml");

            Assert.AreEqual(options.JSLintOptions.BoolOptions, JSLintBoolOption.OptionsUpgraded);
            Assert.AreEqual(options.JSLintOptions.BoolOptions2.Count, 30);
            Assert.AreEqual(options.JSLintOptions.SelectedLinter, Linters.JSLint);
            foreach(KeyValuePair<string,bool> kvp in options.JSLintOptions.BoolOptions2) 
            {
                Assert.AreEqual(kvp.Value, true);
            }
        }

        [TestMethod]
        public void TestLoadingNoErrors1_2_4()
        {
            Options options = LoadOptionsFromEmbeddedResource("SettingFiles._1_2_4.NOErrors.xml");

            Assert.AreEqual(options.JSLintOptions.BoolOptions2.Count, 30);
            Assert.AreEqual(options.JSLintOptions.SelectedLinter, Linters.JSLintOld);
            foreach (KeyValuePair<string, bool> kvp in options.JSLintOptions.BoolOptions2)
            {
                Assert.AreEqual(kvp.Value, false);
            }
        }
    }
}
