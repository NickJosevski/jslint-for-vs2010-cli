﻿var extractRepeatingVars = function (what, context) {
    var v = what.arity;
    if (v === 'string' || v === 'number') {
        v = what.value;
        if (v !== 1 && v !== 0 && v !== '') {
            if (v in context) {
                context[v].count++;
            } else {
                context[v] = { count: 1, line: what.line };
            }
        }
    }
};

var processRepeatingVars = function (strs) {
    var now = 2,
		next,
		k,
		v, 
        constants = [];
    while (now < Number.MAX_VALUE) {
        next = Number.MAX_VALUE;
        for (k in strs) {
            if (strs.hasOwnProperty(k)) {
                v = strs[k];
                if (v.count === now) {
                    constants.push({ line: v.line, 
                        reason: 'Repeating constant \'' + k + '\' occurs ' + v.count + ' times.' });
                } else {
                    if (v.count > now) {
                        next = Math.min(next, v.count);
                    }
                }
            }
        }
        now = next;
    }
    return constants;
};

