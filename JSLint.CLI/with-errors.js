﻿/// <reference path="/Scripts/jquery-1.6.2-vsdoc.js"/>
/// <reference path="/Scripts/jquery.validate-vsdoc.js"/>

function Daedalus() {
};

Daedalus.prototype.startCountDownAndSendData = function (data, waitDuration) {

    function cycle() {
        if (waitDuration <= 0) {
            //$(data).when($('#last-action').append('sent ' + data.val()))
            $('#last-action').append('<br />sent ' + data.val());
            $('form#form0').trigger('submit');
            return;
        }
        waitDuration--;
        setTimeout(cycle, 1e3);
    }
    cycle();
};

Daedalus.prototype.activeInputs = {};

Daedalus.prototype.greenCheckGif = function () {
    return 'http://nickjosevski.files.wordpress.com/2011/04/check1.png';
};
Daedalus.prototype.busyGif = function () {
    return 'http://nickjosevski.wordpress.com/wp-admin/images/wpspin_light.gif';
};
Daedalus.prototype.redXGif = function () {
    return 'http://upload.wikimedia.org/wikipedia/commons/7/7e/Red_x.png';
};


Daedalus.prototype.autoSaveSetup = function () {
    this.activeInputs = {};

    $(':input').change(function () {
        var c = $(this);
        $.when(
                c.focusout()).then(function () {
                    pushToControlList(c);
                    startCountDownAndSendData(c, 5); //delay by 5 seconds
                });
    });
};

Daedalus.prototype.getInputId = function (c) {
    return $(c).attr('id');
};

Daedalus.prototype.showSuccess = function (data) {
    alert('boom');
    //$('#fast-bind').html(data);
};

Daedalus.prototype.displayAjaxError = function (data) {
    alert('ajax error');
};

Daedalus.prototype.pushToControlList = function (c) {
    var elemId = this.getInputId(c);
    var countVal = this.pushModifiedControlOnProcessingStack(elemId);

    $('#fast-span').append('added ' + elemId + ' with # ' + countVal + '<br />');
};

Daedalus.prototype.pushModifiedControlOnProcessingStack = function (elemId) {
    var countVal = 1;
    if (this.activeInputs[elemId] == null)
        this.activeInputs[elemId] = 1;
    else {
        this.activeInputs[elemId] = countVal = this.activeInputs[elemId] + 1;
    }
    return countVal;
};

Daedalus.prototype.popProcessedControlFromStack = function (elemId) {
    var countVal = -1;

    if (this.activeInputs[elemId] == null)
        return -1; //error state

    if (this.activeInputs[elemId] > 1) {
        countVal = this.activeInputs[elemId];
        this.activeInputs[elemId] = this.activeInputs[elemId] - 1;
    }
    else {
        countVal = this.activeInputs[elemId];
        //is this enough? we check for null in the push method, so for now ok...
        this.activeInputs[elemId] = null;
    }
    return countVal;
};

Daedalus.prototype.startCountDownAndSendData = function (c, waitDuration) {
    function go() {
        if (waitDuration <= 0) {
            $('#la-text').append('[' + c.attr('name') + '] = ' + c.val() + '<br />');

            if (popProcessedControlFromStack(getInputId(c)) == 1) {
                //if value "1" - active control then submit, otherwise it was edited recently                
                $('#fast-span').append('POST ' + getInputId(c) + ' with data: ' + c.val() + '<br />');
                //$('form#form0').trigger('submit');
                postFieldForActivity(c);
            }
            return;
        }
        waitDuration--;
        setTimeout(go, 1000);
    }
    go();
};

Daedalus.prototype.postFieldForActivity = function (c) {
    //TODO: extract guid from page for 'id' param
    var itemGuid = '124DFFD3-0FE7-4C9F-BB85-4B4CC903B2C0';

    var activityChanges =
        $.toJSON({
            'id': itemGuid,
            'fieldName': c.attr('name'),
            'data': c.val()
        });
    this.setupAsBusy(c);
    $.ajax({
        url: '/Activity/UpdateActivityField', async: true,
        type: 'POST',
        success: function (d, ts, jq) { handleSucess(d, ts, jq); },
        error: function (jq, status, error) { handleErorr(jq, status, error); },
        dataType: 'json',
        data: activityChanges,
        contentType: 'application/json'
    });
};

Daedalus.prototype.handleErorr = function (jq, status, error) {
    alert('jq = ' + jq + '\r\nstatus:' + status + '\r\nerr:' + error);
};

Daedalus.prototype.setupAsBusy = function (c) {
    //need a neater way to do this?
    var id = c.attr('name') + '-loading';
    $(c).after('<img id="' + id + '" src="' + this.busyGif() + '" alt="busy" />');
};

Daedalus.prototype.handleSucess = function (d, ts, jq) {
    var id = '#' + d.FieldName + '-loading';

    //sharp-replace-of-busy-with-check-fade-out-check
    $(id).attr('src', this.greenCheckGif())
                .fadeIn(2000, function () {
                    $(this).fadeOut(4000);
                });
};
