﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using JSLint.VS2010.OptionsUI.HelperClasses;
using JSLint.VS2010.OptionClasses;
using System.Windows;

namespace JSLint.VS2010.OptionsUI.ViewModel
{
    public class LinterViewModel : ViewModelBase
    {
        //private SettingsViewModel _jslintsettings = null;
        private LinterModel _linter = null;
        private ObservableCollection<LintBooleanSettingViewModel> _booleanSettings;
        private JSLintOptions _jslintOptions;

        public ObservableCollection<LintBooleanSettingViewModel> Settings
        {
            get { return _booleanSettings; }
        }

        public string LinterName
        {
            get { return _linter.Name; }
        }

        public Visibility TreeAvailableOption
        {
            get { return Visibility.Hidden; } // TODO - fix tree if (_linter.SupportsTree) { return Visibility.Visible; } else { return Visibility.Hidden; } }
        }

        internal void TriggerSettingsChange()
        {
            //OnPropertyChanged("Settings");
            foreach (LintBooleanSettingViewModel vm in Settings)
            {
                vm.On = vm.On;
            }
        }

        public LinterViewModel(LinterModel linterModel, JSLintOptions jslintOptions)
        {
            _linter = linterModel;
            //_jslintsettings = settingsvm;
            _jslintOptions = jslintOptions;
            _booleanSettings = new ObservableCollection<LintBooleanSettingViewModel>(GetBooleanSettings(_linter, _jslintOptions));
        }

        public IEnumerable<LintBooleanSettingViewModel> GetBooleanSettings(LinterModel linter, JSLintOptions jslintoptions)
        {
            return LintBooleanSettingModel.AllOptions
                .Where(a => ((a.LintersAppliesTo & linter.Type) > 0))
                .Select(a => new LintBooleanSettingViewModel(a, jslintoptions));
        }

        public Linters LinterType 
        {
            get
            {
                return _linter.Type;
            }
        }
    }
}
