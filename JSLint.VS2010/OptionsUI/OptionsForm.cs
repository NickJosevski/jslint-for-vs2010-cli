﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using JSLint.VS2010.OptionClasses;

namespace JSLint.VS2010.OptionsUI
{
    public partial class OptionsForm : Form
    {
        private static string _title = string.Concat(
                "JSLint for Visual Studio 2010 v",
                typeof(OptionsForm).Assembly.GetName().Version.ToString());

        public OptionsForm()
        {
            Options.ReloadCurrent();

            InitializeComponent();
        }

        private void LoadOptions(Options options)
        {
            optionsUI.LoadOptions(options);

            optionsUI.IsEnabled =
                enableJSLint.Checked
                = options.Enabled;
        }

        private void SaveOptions(Options dest)
        {
            dest.Enabled = enableJSLint.Checked;
            optionsUI.SaveOptions(dest);
        }

        private void enableJSLint_CheckedChanged(object sender, EventArgs e)
        {
            optionsUI.IsEnabled = enableJSLint.Checked;
        }

        private void OptionsForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Escape)
            {
                Hide();
            }
        }

        private void import_Click(object sender, EventArgs e)
        {
            if (importFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Options options = Options.Load(importFileDialog.FileName);
                    LoadOptions(options);
                }
                catch (Exception x)
                {
                    MessageBox.Show(
                        x.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void export_Click(object sender, EventArgs e)
        {
            if (exportFileDialog.ShowDialog() == DialogResult.OK)
            {
                Options temp = new Options();
                SaveOptions(temp);
                temp.SaveAs(exportFileDialog.FileName);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.linkLabel1.LinkVisited = true;

            Process.Start("http://www.jslint.com/");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.linkLabel2.LinkVisited = true;

            Process.Start("http://jslint4vs2010.codeplex.com/");
        }

        private void ok_Click(object sender, EventArgs e)
        {
            SaveOptions(Options.Current);

            Options.Current.SaveChanges();

            Hide();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void OptionsForm_Load(object sender, EventArgs e)
        {
            Text = _title;

            KeyPreview = true;

            LoadOptions(Options.Current);
        }
    }
}

