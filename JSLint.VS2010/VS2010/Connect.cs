using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml.Serialization;

using EnvDTE;
using EnvDTE80;

using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;
using Microsoft.VisualStudio.Editor;
using Microsoft.VisualStudio.Text;
using JSLint.VS2010.LinterBridge;
using JSLint.VS2010.OptionClasses;
using JSLint.VS2010.OptionsUI;

namespace JSLint.VS2010.VS2010
{
    static class GuidList
    {
        public const string guidPkgString = "531da20f-aec5-4382-a317-0f70a217a572";
        public const string guidSourceEditorCmdSetString = "36c9360d-beb4-4ee0-9e7c-75264224c59c";
        public const string guidSourceEditorFragmentCmdSetString = "45b15f14-e401-462c-8eb0-5fcab4cc3195";
        public const string guidSolutionItemCmdSetString = "d9032052-cbd8-4f67-8352-e183f74e4071";
        public const string guidSolutionFolderNodeCmdSetString = "11ecda11-e5ee-4e59-a4e1-ef9edec80d8f";
        public const string guidOptionsCmdSetString = "b7fe589c-403a-4bfa-88e2-795a77b2d5b3";
        public const string guidErrorListCmdString = "284b5171-6b47-4cbf-a617-ab8b7d113725";

        public static readonly Guid guidSourceEditorCmdSet = new Guid(guidSourceEditorCmdSetString);
        public static readonly Guid guidSourceEditorFragmentCmdSet = new Guid(guidSourceEditorFragmentCmdSetString);
        public static readonly Guid guidSolutionItemCmdSet = new Guid(guidSolutionItemCmdSetString);
        public static readonly Guid guidSolutionFolderNodeCmdSet = new Guid(guidSolutionFolderNodeCmdSetString);
        public static readonly Guid guidOptionsCmdSet = new Guid(guidOptionsCmdSetString);
        public static readonly Guid guidErrorListCmdSet = new Guid(guidErrorListCmdString);
    }

    static class PkgCmdIDList
    {
        public const uint lint = 0x100;
        public const uint options = 0x100;
        public const uint wipeerrors = 0x100;
        public const uint exclude = 0x101;
        public const uint excludeFolder = 0x100;
        public const uint globals = 0x102;
    }

    [PackageRegistration(UseManagedResourcesOnly = true)]
    // This attribute is used to register the informations needed to show the this package
    // in the Help/About dialog of Visual Studio.
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    // This attribute is needed to let the shell know that this package exposes some menus.
    [ProvideMenuResource("Menus.ctmenu", 1)]
    // --- Microsoft.VisualStudio.VSConstants.UICONTEXT_NoSolution
    [ProvideAutoLoad("ADFC4E64-0397-11D1-9F4E-00A0C911004F")]
    // --- Microsoft.VisualStudio.VSConstants.UICONTEXT_HasSolution
    [ProvideAutoLoad("f1536ef8-92ec-443c-9ed7-fdadf150da82")]
    [Guid(GuidList.guidPkgString)]
    public sealed class Connect2 :
        Package,
        IDisposable
    {
        private DTE2 _dte2;

        private BuildEvents _buildEvents; // needs to be a field otherwise gc'd
		private DocumentEvents _docEvents; // needs to be a field otherwise gc'd

        private vsBuildScope _buildScope;
        private vsBuildAction _buildAction;

        private ErrorListHelper _errorListHelper;
        private JSLinter _linter = new JSLinter();

        private int _errorCount;
        private const int Threshold = 1000;

        private Dictionary<string, List<string>> _skippedNodes =
            new Dictionary<string, List<string>>(8);

        public Connect2()
        {
        }

        protected override void Initialize()
        {
            base.Initialize();

            Options.ReloadCurrent();

            _dte2 = GetService(typeof(DTE)) as DTE2;
            _buildEvents = _dte2.Events.BuildEvents;
			_docEvents = _dte2.Events.DocumentEvents;
            _errorListHelper = new ErrorListHelper(_dte2);

            // Add our command handlers for menu (commands must exist in the .vsct file)
            OleMenuCommandService mcs = GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (null != mcs)
            {
                // Source Editor: JS Lint
                CommandID sourceEditorLintCmdID = new CommandID(GuidList.guidSourceEditorCmdSet, (int)PkgCmdIDList.lint);
                MenuCommand sourceEditorLintMenuItem = new MenuCommand(LintSourceEditorCmdCallback, sourceEditorLintCmdID);
                mcs.AddCommand(sourceEditorLintMenuItem);

                // Source Editor: JS Lint Fragment
                CommandID sourceEditorFragmentLintCmdID = new CommandID(GuidList.guidSourceEditorFragmentCmdSet, (int)PkgCmdIDList.lint);
                OleMenuCommand sourceEditorFragmentLintMenuItem = new OleMenuCommand(LintSourceEditorFragmentItemCmdCallback, sourceEditorFragmentLintCmdID);
                sourceEditorFragmentLintMenuItem.BeforeQueryStatus += sourceEditorFragmentLintMenuItem_BeforeQueryStatus;
                mcs.AddCommand(sourceEditorFragmentLintMenuItem);

                // Solution Explorer: JS Lint
                CommandID solutionItemCmdID = new CommandID(GuidList.guidSolutionItemCmdSet, (int)PkgCmdIDList.lint);
                OleMenuCommand solutionItemMenuItem = new OleMenuCommand(LintSolutionItemCmdCallback, solutionItemCmdID);
                solutionItemMenuItem.BeforeQueryStatus += solutionItemMenuItem_BeforeQueryStatus;
                mcs.AddCommand(solutionItemMenuItem);

                // Solution Explorer: Skip File
                CommandID solutionItemSkipCmdID = new CommandID(GuidList.guidSolutionItemCmdSet, (int)PkgCmdIDList.exclude);
                OleMenuCommand solutionItemSkipMenuItem = new OleMenuCommand(LintSolutionItemSkipCmdCallback, solutionItemSkipCmdID);
                solutionItemSkipMenuItem.BeforeQueryStatus += solutionItemSkipMenuItem_BeforeQueryStatus;
                mcs.AddCommand(solutionItemSkipMenuItem);

                // Solution Explorer: Skip Folder
                CommandID solutionFolderNodeSkipCmdID = new CommandID(GuidList.guidSolutionFolderNodeCmdSet, (int)PkgCmdIDList.excludeFolder);
                OleMenuCommand solutionFolderNodeSkipMenuItem = new OleMenuCommand(LintSolutionFolderNodeSkipCmdCallback, solutionFolderNodeSkipCmdID);
                solutionFolderNodeSkipMenuItem.BeforeQueryStatus += solutionFolderNodeSkipMenuItem_BeforeQueryStatus;
                mcs.AddCommand(solutionFolderNodeSkipMenuItem);

                // Solution Explorer: Predefined global variables
                CommandID solutionItemGlobalsCmdID = new CommandID(GuidList.guidSolutionItemCmdSet, (int)PkgCmdIDList.globals);
                OleMenuCommand solutionItemGlobalsMenuItem = new OleMenuCommand(LintSolutionItemGlobalsCmdCallback, solutionItemGlobalsCmdID);
                solutionItemGlobalsMenuItem.BeforeQueryStatus += solutionItemGlobalsMenuItem_BeforeQueryStatus;
                mcs.AddCommand(solutionItemGlobalsMenuItem);

                // Source Editor: Predefined global variables
                CommandID sourceEditorGlobalsCmdID = new CommandID(GuidList.guidSourceEditorCmdSet, (int)PkgCmdIDList.globals);
                MenuCommand sourceEditorGlobalsMenuItem = new MenuCommand(LintSourceEditorGlobalsCmdCallback, sourceEditorGlobalsCmdID);
                mcs.AddCommand(sourceEditorGlobalsMenuItem);

                // Error List: Clear JS Lint Errors
                CommandID errorListCmdID = new CommandID(GuidList.guidErrorListCmdSet, (int)PkgCmdIDList.wipeerrors);
                OleMenuCommand errorListMenuItem = new OleMenuCommand(ErrorListCmdCallback, errorListCmdID);
                errorListMenuItem.BeforeQueryStatus += errorListMenuItem_BeforeQueryStatus;
                mcs.AddCommand(errorListMenuItem);

                // Main Menu: JSLint Options
                CommandID optionsCmdID = new CommandID(GuidList.guidOptionsCmdSet, (int)PkgCmdIDList.options);
                MenuCommand optionsMenuItem = new MenuCommand(OptionsCmdCallback, optionsCmdID);
                mcs.AddCommand(optionsMenuItem);
            }

            // build events
            _buildEvents.OnBuildBegin += buildEvents_OnBuildBegin;
            _buildEvents.OnBuildProjConfigBegin += buildEvents_OnBuildProjConfigBegin;

            _docEvents.DocumentSaved += DocumentEvents_DocumentSaved;
        }

        void DocumentEvents_DocumentSaved(Document document)
        {
            if  (Options.Current.RunOnSave && document != null) {
                IncludeFileType fileType = GetFileType(document.FullName);
				if ((fileType & Options.Current.SaveFileTypes) > 0)
				{
					ResetErrorCount();
					SuspendErrorList();

					bool bDontCare;
					ClearErrors(document.FullName);
					AnalyzeFile(document.FullName, out bDontCare);
					
					ResumeErrorList(false);
				}
            }
        }

        private void solutionItemMenuItem_BeforeQueryStatus(object sender, EventArgs e)
        {
            OleMenuCommand menuCommand = sender as OleMenuCommand;
            if (menuCommand != null)
            {
                var activeItems = ActiveUIHierarchyItems;
                foreach (UIHierarchyItem item in activeItems)
                {
					IncludeFileType fileType = GetFileType(item.Name);

                    if (fileType == IncludeFileType.None ||
						fileType == IncludeFileType.Folder )
                    {
                        menuCommand.Visible = false;

                        return;
                    }
                }

                menuCommand.Visible = true;
            }
        }

        private void solutionItemSkipMenuItem_BeforeQueryStatus(object sender, EventArgs e)
        {
            OleMenuCommand menuCommand = sender as OleMenuCommand;
            if (menuCommand != null)
            {
				menuCommand.Visible = true;

                var activeItems = ActiveUIHierarchyItems;
                foreach (UIHierarchyItem item in activeItems)
                {
					IncludeFileType fileType = GetFileType(item.Name);

                    if ((Options.Current.BuildFileTypes & fileType) == 0)
                    {
                        menuCommand.Visible = false;

                        return;
                    }
                }

                menuCommand.Checked = false;
                menuCommand.Enabled = true;
                foreach (UIHierarchyItem item in activeItems)
                {
                    bool skippedByFolder;
                    if (IsNodeSkipped((ProjectItem)item.Object, out skippedByFolder))
                    {
                        menuCommand.Checked = true;
                        if (skippedByFolder)
                        {
                            menuCommand.Enabled = false;
                        }

                        return;
                    }
                }
            }
        }

        private void solutionFolderNodeSkipMenuItem_BeforeQueryStatus(object sender, EventArgs e)
        {
            OleMenuCommand menuCommand = sender as OleMenuCommand;
            menuCommand.Visible = true;
            if (menuCommand != null)
            {
                var activeItems = ActiveUIHierarchyItems;
                menuCommand.Checked = false;
                foreach (UIHierarchyItem item in activeItems)
                {
                    if (IsNodeSkipped((ProjectItem)item.Object))
                    {
                        menuCommand.Checked = true;

                        return;
                    }
                }
            }
        }

        private void solutionItemGlobalsMenuItem_BeforeQueryStatus(object sender, EventArgs e)
        {
            OleMenuCommand menuCommand = sender as OleMenuCommand;
            if (menuCommand != null)
            {
                var activeItems = ActiveUIHierarchyItems;
                if (activeItems.Length != 1)
                {
                    menuCommand.Visible = false;

                    return;
                }

                menuCommand.Visible = GetFileType(((ProjectItem)ActiveUIHierarchyItem.Object).Name) == IncludeFileType.JS;
            }
        }

        private void sourceEditorFragmentLintMenuItem_BeforeQueryStatus(object sender, EventArgs e)
        {
            OleMenuCommand menuCommand = sender as OleMenuCommand;
            if (menuCommand != null)
            {
                string selection = ActiveTextDocument.Selection.Text;

                menuCommand.Visible = selection.Length > 0 && (GetFileType(_dte2.ActiveDocument.FullName, selection) != IncludeFileType.None);
            }
        }

        private void errorListMenuItem_BeforeQueryStatus(object sender, EventArgs e)
        {
            OleMenuCommand menuCommand = sender as OleMenuCommand;
            if (menuCommand != null)
            {
                menuCommand.Visible = Options.Current.ErrorCategory.IsTaskError() &&
                    _errorListHelper != null &&
                    _errorListHelper.ErrorCount > 0;
            }
        }

        private void LintSolutionItemCmdCallback(object sender, EventArgs e)
        {
            ResetErrorCount();
            SuspendErrorList();

            foreach (var item in ActiveUIHierarchyItems)
            {
                ProjectItem projItem = (ProjectItem)((UIHierarchyItem)item).Object;

                ClearErrors(projItem.FileNames[0]);
                bool reachedTreshold;
                AnalyzeFile(projItem.FileNames[0], out reachedTreshold);
                if (reachedTreshold)
                {
                    break;
                }
            }

            ResumeErrorList();
        }

        private void LintSolutionItemSkipCmdCallback(object sender, EventArgs e)
        {
            foreach (var item in ActiveUIHierarchyItems)
            {
                ProjectItem projItem = (ProjectItem)((UIHierarchyItem)item).Object;
				IncludeFileType fileType = GetFileType(projItem.Name);

                if ((Options.Current.BuildFileTypes & fileType) > 0)
                {
                    ToggleSkip(projItem);
                }
            }
        }

        private void LintSolutionFolderNodeSkipCmdCallback(object sender, EventArgs e)
        {
            foreach (var item in ActiveUIHierarchyItems)
            {
                ToggleSkip((ProjectItem)((UIHierarchyItem)item).Object);
            }
        }

        private void GotoGlobals(TextDocument doc)
        {
            doc.Selection.MoveToLineAndOffset(1, 1);
            if (doc.Selection.FindText("/*globals"))
            {
                doc.Selection.SelectLine();
            }
            else
            {
                doc.CreateEditPoint().Insert("/*globals _comma_separated_list_of_variables_*/\r\n");
                doc.Selection.MoveToLineAndOffset(1, 1);
                doc.Selection.FindText("_comma_separated_list_of_variables_");
            }
        }

        private void LintSolutionItemGlobalsCmdCallback(object sender, EventArgs e)
        {
            var item = ActiveUIHierarchyItem;
            ProjectItem projItem = (ProjectItem)((UIHierarchyItem)item).Object;
            projItem.Open().Document.Activate();

            GotoGlobals(ActiveTextDocument);
        }

        private void LintSourceEditorGlobalsCmdCallback(object sender, EventArgs e)
        {
            GotoGlobals(ActiveTextDocument);
        }

        private void LintSourceEditorFragmentItemCmdCallback(object sender, EventArgs e)
        {
            if (ActiveTextDocument.Selection.Text.Length > 0)
            {
                SuspendErrorList();

                string filename = _dte2.ActiveDocument.FullName;
                ClearErrors(filename);

                AnalyzeFragment(ActiveTextDocument.Selection.Text, filename, ActiveTextDocument.Selection.TopPoint.Line, ActiveTextDocument.Selection.TopPoint.DisplayColumn);

                ResumeErrorList();
            }
        }

        private void LintSourceEditorCmdCallback(object sender, EventArgs e)
        {
            SuspendErrorList();

            IWpfTextView view = GetActiveTextView();
            ITextDocument document;
            view.TextDataModel.DocumentBuffer.Properties.TryGetProperty(typeof(ITextDocument), out document);
            string filename = document != null ? document.FilePath : "";

            ClearErrors(filename);

            string selectionText = ActiveTextDocument.Selection.Text;

            if (!String.IsNullOrWhiteSpace(selectionText))
            {
                AnalyzeFragment(selectionText, filename, ActiveTextDocument.Selection.TopPoint.Line, ActiveTextDocument.Selection.TopPoint.DisplayColumn);
            }
            else
            {
                string lintFile = view.TextBuffer.CurrentSnapshot.GetText();
                
                AnalyzeFragment(lintFile, filename);
            }

            ResumeErrorList();
        }

        private void ErrorListCmdCallback(object sender, EventArgs e)
        {
            if (Options.Current.ErrorCategory.IsTaskError())
            {
                if (_errorListHelper != null)
                {
                    _errorListHelper.Clear();
                    ResetErrorCount();
                }
            }
        }

        private void OptionsCmdCallback(object sender, EventArgs e)
        {
            using (OptionsForm optionsForm = new OptionsForm())
            {
                optionsForm.ShowDialog();
            }
        }

        private void buildEvents_OnBuildBegin(vsBuildScope Scope, vsBuildAction Action)
        {
            _buildScope = Scope;
            _buildAction = Action;

            if (_errorListHelper != null)
            {
                _errorListHelper.Clear();
            }

        }

        private void buildEvents_OnBuildProjConfigBegin(
            string Project,
            string ProjectConfig,
            string Platform,
            string SolutionConfig)
        {
            if (_buildAction == vsBuildAction.vsBuildActionClean ||
                !Options.Current.Enabled ||
                !Options.Current.RunOnBuild)
            {
                return;
            }

            var proj = _dte2.Solution.AllProjects().Single(p => p.UniqueName == Project);

            ResetErrorCount();
            SuspendErrorList();

            bool reachedTreshold;
            AnalyzeProjectItems(proj.ProjectItems, out reachedTreshold);

            ResumeErrorList();
            UpdateStatusBar(reachedTreshold);


            if (_errorCount > 0 && Options.Current.CancelBuildOnError)
            {
                WriteToErrorList("Build cancelled due to JSLint validation errors.");
                _dte2.ExecuteCommand("Build.Cancel");
            }
        }

        private TextDocument ActiveTextDocument
        {
            get
            {
                return (TextDocument)_dte2.ActiveDocument.Object("TextDocument");
            }
        }

        private IWpfTextView GetActiveTextView()
        {
            IWpfTextView view = null;
            IVsTextView vTextView = null;
            IVsTextManager txtMgr = (IVsTextManager)GetService(typeof(SVsTextManager));
            int mustHaveFocus = 1;
            txtMgr.GetActiveView(mustHaveFocus, null, out vTextView);

            IVsUserData userData = vTextView as IVsUserData;

            if (null != userData)
            {
                IWpfTextViewHost viewHost;
                object holder;
                Guid guidViewHost = DefGuidList.guidIWpfTextViewHost;
                userData.GetData(ref guidViewHost, out holder);
                viewHost = (IWpfTextViewHost)holder;
                view = viewHost.TextView;
            }
            return view;
        }

        private Array ActiveUIHierarchyItems
        {
            get
            {
                UIHierarchy h = (UIHierarchy)_dte2.ToolWindows.GetToolWindow(
                    EnvDTE.Constants.vsWindowKindSolutionExplorer);
                return (Array)h.SelectedItems;
            }
        }

        private UIHierarchyItem ActiveUIHierarchyItem
        {
            get
            {
                return (UIHierarchyItem)ActiveUIHierarchyItems.GetValue(0);
            }
        }

        private IVsBuildPropertyStorage GetVsBuildPropertyStorage(Project proj)
        {
            IVsSolution sln = GetService(typeof(SVsSolution)) as IVsSolution;
            var activeProjs = (Array)_dte2.ActiveSolutionProjects;
            IVsHierarchy hierarchy;
            sln.GetProjectOfUniqueName(proj.FullName, out hierarchy);

            return hierarchy as IVsBuildPropertyStorage;
        }

        private const string WebsiteKind = "{E24C65DC-7377-472b-9ABA-BC803B73C61A}";

        private string GetCustomProperty(string name, Project proj)
        {
            if (!WebsiteKind.Equals(proj.Kind, StringComparison.Ordinal))
            {
                var storage = GetVsBuildPropertyStorage(proj);
                string value;
                storage.GetPropertyValue(
                    name,
                    string.Empty, (uint)_PersistStorageType.PST_PROJECT_FILE,
                    out value);

                return value;
            }

            return GetWebsiteCustomProperty<string>(name, proj);
        }

        private void SetCustomProperty(string name, string value, Project proj)
        {
            if (!WebsiteKind.Equals(proj.Kind, StringComparison.Ordinal))
            {
                var storage = GetVsBuildPropertyStorage(proj);
                ErrorHandler.ThrowOnFailure(
                    storage.SetPropertyValue(
                        name, string.Empty, (uint)_PersistStorageType.PST_PROJECT_FILE, value));
            }
            else
            {
                SetWebsiteCustomProperty<string>(name, value, proj);
            }
        }

        private static T GetWebsiteCustomProperty<T>(string name, Project proj)
        {
            string cacheDir = Utility.GetWebsiteCacheFolder(proj);
            if (cacheDir != null)
            {
                string filename = string.Concat(cacheDir, name, ".xml");
                if (File.Exists(filename))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    using (FileStream fin = File.OpenRead(filename))
                    {
                        return (T)serializer.Deserialize(fin);
                    }
                }
            }

            return default(T);
        }

        private static void SetWebsiteCustomProperty<T>(string name, T value, Project proj)
        {
            string cacheDir = Utility.GetWebsiteCacheFolder(proj);
            if (cacheDir != null)
            {
                string filename = string.Concat(cacheDir, name, ".xml");
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                using (FileStream fout = File.Create(filename))
                {
                    serializer.Serialize(fout, value);
                }
            }
        }

        private List<string> GetSkippedNodes(Project proj)
        {
            List<string> skipped;
            if (!_skippedNodes.TryGetValue(proj.FullName, out skipped))
            {
                skipped = new List<string>(8);
                _skippedNodes.Add(proj.FullName, skipped);

                string value = GetCustomProperty("JSLintSkip", proj);
                if (value != null)
                {
                    skipped.AddRange(value.Split('|'));
                }
            }

            return skipped;
        }

        private static string GetRelativeName(ProjectItem projItem)
        {
            return projItem.FileNames[0].Substring(
                Path.GetDirectoryName(projItem.ContainingProject.FullName).Length);
        }

        private bool IsNodeSkipped(ProjectItem projItem, out bool skippedByFolder)
        {
            skippedByFolder = false;
            string name = GetRelativeName(projItem);
            foreach (var skipped in GetSkippedNodes(projItem.ContainingProject))
            {
                if (skipped.Length > 0 && name.StartsWith(skipped))
                {
                    if (name.Length > skipped.Length)
                    {
                        skippedByFolder = true;
                    }

                    return true;
                }
            }

            return false;
        }

        private bool IsNodeSkipped(ProjectItem projItem)
        {
            bool skippedByFolder;
            return IsNodeSkipped(projItem, out skippedByFolder);
        }

        private void ToggleSkip(ProjectItem projItem)
        {
            var proj = projItem.ContainingProject;
            var skipped = GetSkippedNodes(proj);
            string name = GetRelativeName(projItem);
            if (skipped.Contains(name))
            {
                skipped.Remove(name);
            }
            else
            {
                skipped.Add(name);
            }

            SetCustomProperty("JSLintSkip", string.Join("|", skipped), proj);
        }

        private void AnalyzeProjectItems(ProjectItems projItems, out bool reachedTreshold)
        {
            reachedTreshold = false;
            for (int i = 1; i <= projItems.Count; i++)
            {
                ProjectItem item = projItems.Item(i);
				
				IncludeFileType fileType = GetFileType(item.FileNames[0]);

                if (fileType == IncludeFileType.Folder) // folder
                {
                    AnalyzeProjectItems(item.ProjectItems, out reachedTreshold);
                }
                else if ((Options.Current.BuildFileTypes & fileType) > 0 &&
                    item.FileCount == 1 &&
                    !IsNodeSkipped(item))
                {
                    ClearErrors(item.FileNames[0]);
                    AnalyzeFile(item.FileNames[0], out reachedTreshold);
                }

                if (reachedTreshold)
                {
                    break;
                }
            }
        }

        private void AnalyzeFile(string filename, out bool reachedThreshold)
        {
			try
			{
				string text = File.ReadAllText(filename);
				Analyze(text, filename);
			}
			catch (Exception e)
			{
				WriteToErrorList(e.Message);
			}
			finally
			{
				reachedThreshold = _errorCount > Threshold;

                if (reachedThreshold)
                {
                    WriteToErrorList(string.Format("Error threshold of {0} reached. JSLint will not generate any more errors for this operation.", Threshold));
                }
			}
        }

		private void Analyze(string fragment, string filename, int lineOffset = 1, int charOffset = 1)
		{
            if (String.IsNullOrWhiteSpace(fragment))
            {
                return;
            }

			try
            {
				int firstLineOfFragment = 0;
                IncludeFileType fileType = GetFileType(filename, fragment);

                if (fileType == IncludeFileType.CSS)
				{
					if (!fragment.StartsWith("@charset"))
					{
						if (lineOffset > 1 || Options.Current.FakeCSSCharset)
						{
							fragment = "@charset \"UTF-8\";" + "\n" + fragment;
							firstLineOfFragment = 1;
						}
						else
						{
							WriteError(
								"",
								lineOffset,
								1,
								"CSS Files must begin @charset to be parsed by JS Lint",
								filename);
							return;
						}
					}
				}

				IgnoreErrorSectionsHandler ignoreErrorHandler = new IgnoreErrorSectionsHandler(fragment);

                bool isJS = fileType == IncludeFileType.JS;
                var errors = _linter.Lint(fragment, Options.Current.JSLintOptions, isJS);
				foreach (var error in errors)
				{
					if (ignoreErrorHandler.IsErrorIgnored(error.Line, error.Column))
					{
						continue;
					}

					if (++_errorCount > Threshold)
					{
						break;
					}

					WriteError(
						error.Evidence,
						error.Line + lineOffset - (1 + firstLineOfFragment),
						error.Line == 1 + firstLineOfFragment
							? error.Column + charOffset - 1
							: error.Column,
						error.Message,
						filename,
                        !isJS);
				}

                if (Options.Current.TODOEnabled && isJS)
                {
                    var todos = TodoFinder.FindTodos(fragment);

                    foreach (var error in todos)
                    {
                        WriteTODO(
                            error.Line + lineOffset - (1 + firstLineOfFragment),
                            error.Line == 1 + firstLineOfFragment
                                ? error.Column + charOffset - 1
                                : error.Column,
                            error.Message,
                            filename);
                    }
                }
            }
            catch (Exception e)
            {
                WriteToErrorList(e.Message);
            }
		}

        private void AnalyzeFragment(string fragment, string filename, int lineOffset = 1, int charOffset = 1)
        {
            ResetErrorCount();

			Analyze(fragment, filename, lineOffset, charOffset);

			UpdateStatusBar(_errorCount > Threshold);
        }

		private IncludeFileType GetFileType(string filename, string fragment = null)
		{
			if (filename.EndsWith(".js", StringComparison.InvariantCultureIgnoreCase))
			{
				return IncludeFileType.JS;
			}

			if (filename.EndsWith(".css", StringComparison.InvariantCultureIgnoreCase))
			{
				return IncludeFileType.CSS;
			}

			if (filename.EndsWith(".htm", StringComparison.InvariantCultureIgnoreCase) ||
				filename.EndsWith(".html", StringComparison.InvariantCultureIgnoreCase) ||
                filename.EndsWith(".aspx", StringComparison.InvariantCultureIgnoreCase) ||
                filename.EndsWith(".ascx", StringComparison.InvariantCultureIgnoreCase))
			{
                if (fragment != null && !fragment.TrimStart().StartsWith("<"))
                {
                    return IncludeFileType.JS;
                }
                else
                {
                    if (filename.EndsWith(".htm", StringComparison.InvariantCultureIgnoreCase) ||
                        filename.EndsWith(".html", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return IncludeFileType.HTML;
                    }
                }
			}

			if (filename.EndsWith(Path.DirectorySeparatorChar.ToString(), StringComparison.InvariantCultureIgnoreCase))
			{
				return IncludeFileType.Folder;
			}

			return IncludeFileType.None;
		}

        private void WriteToErrorList(string message)
        {
            _errorListHelper.Write(
                Microsoft.VisualStudio.Shell.TaskCategory.BuildCompile,
                Microsoft.VisualStudio.Shell.TaskErrorCategory.Error,
                message, string.Empty, 0, 0);
        }

        private OutputWindowPane GetOutputWindowPane()
        {
            const string BuildOutputPaneGuid = "{1BD8A850-02D1-11D1-BEE7-00A0C913D1F8}";

            OutputWindow outputWindow = (_dte2.Windows.Item(EnvDTE.Constants.vsWindowKindOutput)).Object as OutputWindow;
            foreach (OutputWindowPane outputWindowPane in outputWindow.OutputWindowPanes)
            {
                if (outputWindowPane.Guid.Equals(BuildOutputPaneGuid, StringComparison.OrdinalIgnoreCase))
                {
                    return outputWindowPane;
                }
            }

            return null;
        }

        private void WriteTODO(
            int line, int column, string message, string filename)
        {
            Write(Options.Current.TODOCategory, line, column, "TODO", message, filename);
        }


        private void WriteError(
             string evidence, int line, int column, string message, string filename, bool forceJSLint = false)
        {
            string msgFormat;
            if (forceJSLint)
            {
                msgFormat = "JS Lint (Htm/Css): ";
            }
            else
            {
                switch (Options.Current.JSLintOptions.SelectedLinter)
                {
                    case Linters.JSHint:
                        msgFormat = "JS Hint: ";
                        break;
                    default:
                    case Linters.JSLint:
                        msgFormat = "JS Lint: ";
                        break;
                    case Linters.JSLintOld:
                        msgFormat = "JS Lint Old: ";
                        break;
                }
            }
            Write(Options.Current.ErrorCategory, line, column, evidence, string.Concat(msgFormat, message), filename);
        }

        private void Write(ErrorCategory category,
            int line, int column, string subcategory, string message, string filename)
        {
            if (category.IsTaskError())
            {
                _errorListHelper.Write(
                    TaskCategory.BuildCompile,
                    (TaskErrorCategory)category,
                    message, filename, line, column);
            }
            else
            {
                TaskList taskList = (TaskList)(_dte2.Windows.Item(EnvDTE.Constants.vsWindowKindTaskList)).Object;
                taskList.TaskItems.Add(
                    "JSLint", subcategory ?? string.Empty, message,
                    vsTaskPriority.vsTaskPriorityHigh, null, true,
                    filename, line, true, true);
            }
        }

        private void SuspendErrorList()
        {
            if (Options.Current.ErrorCategory.IsTaskError())
            {
                if (_errorListHelper != null)
                {
                    _errorListHelper.SuspendRefresh();
                }
            }
        }

        private void ResumeErrorList(bool focus = true)
        {
            if (Options.Current.ErrorCategory.IsTaskError())
            {
                if (_errorListHelper != null)
                {
                    _errorListHelper.ResumeRefresh(focus);
                }
            }
        }

        private void ClearErrors(string filename)
        {
            if (Options.Current.ErrorCategory.IsTaskError())
            {
                if (_errorListHelper != null)
                {
                    _errorListHelper.ClearDocument(filename);
                }
            }
        }

        private void ResetErrorCount()
        {
            _errorCount = 0;
        }

        private void UpdateStatusBar(bool reachedTreshold)
        {
            _dte2.StatusBar.Text = string.Format("JS Lint: {0}{1} errors",
                _errorCount, reachedTreshold ? "+" : string.Empty);
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_errorListHelper != null)
            {
                _errorListHelper.Dispose();
                _errorListHelper = null;
            }

            if (_linter != null)
            {
                _linter.Dispose();
                _linter = null;
            }
        }

        #endregion
    }
}

