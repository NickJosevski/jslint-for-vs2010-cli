﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JSLint.VS2010.OptionClasses
{
    public class LinterModel
    {
        public Linters Type { get; set; }
        public string Name { get; set; }

        public LinterModel(string name, Linters type, bool supportsTree)
        {
            Name = name;
            Type = type;
            SupportsTree = supportsTree;
        }

        public bool SupportsTree { get; set; }
    }
}
