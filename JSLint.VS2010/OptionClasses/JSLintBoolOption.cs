﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace JSLint.VS2010.OptionClasses
{
	[Flags]
	public enum JSLintBoolOption
	{
		adsafe = 1,
		bitwise = 2,
		browser = 4,
		cap = 8,
		css = 16,
		debug = 32,
		devel = 64,
		eqeqeq = 128,
		es5 = 256,
		evil = 512,
		forin = 1024,
		fragment = 2048,
		immed = 4096,
		laxbreak = 8192,
		newcap = 16384,
		nomen = 16384*2,
		on = 16384*4,
		onevar = 16384*8,
		passfail = 16384*16,
		plusplus = 16384*32,
		regexp = 16384*64,
		rhino = 16384*128,
		undef = 16384*256,
		safe = 16384*512,
		windows = 16384*1024,
		strict = 16384*2048,
		sub = 16384*4096,
		white = 16384*8192,
		widget = 16384*16384,
		tolcontinue = 16384*16384*2,
		OptionsUpgraded = 16384*16384*4
	}
}