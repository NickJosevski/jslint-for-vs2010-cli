﻿using System;
using System.IO;
using System.Xml.Serialization;

using _ErrorCategory = JSLint.VS2010.VS2010.ErrorCategory;
using JSLint.VS2010.VS2010;

namespace JSLint.VS2010.OptionClasses
{
    [XmlRoot]
    public sealed class Options
    {
        internal static readonly string OptionsPath = GetOptions();

        private static Options _current = Options.Load(OptionsPath);

        public static Options Current
        {
            get
            {
                return _current;
            }
        }

        private static string GetOptions()
        {
            using (var f = new StreamWriter(@"C:\nick-output\wtf_is_happening.txt", true))
            {
                f.WriteLine("{0} HERE HERE HERE -- |{1}|", DateTime.Now, Environment.CurrentDirectory);

                var path = Environment.CurrentDirectory + @"\..\jslint\JSLintOptions.xml";

                if (!File.Exists(path))
                {
                    //fall back to AppData\Roaming
                    path = Utility.GetFilename("JSLintOptions.xml");
                }

                f.WriteLine("{0} PATH PATH PATH -- |{1}|", DateTime.Now, path);
                f.Close();
                return path;
            }
        }

        internal Options()
        {
            Enabled = true;
            JSLintOptions = JSLintOptions.Default;
            ErrorCategory = _ErrorCategory.Warning;
            TODOEnabled = false;
            TODOCategory = _ErrorCategory.Task;
            RunOnBuild = true;
            CancelBuildOnError = true;
			FakeCSSCharset = false;
			BuildFileTypes = IncludeFileType.JS;
			IgnoreErrorStart = "/*ignore jslint start*/";
			IgnoreErrorEnd = "/*ignore jslint end*/";
			IgnoreErrorLine = "//ignore jslint";
			RunOnSave = false;
			SaveFileTypes = IncludeFileType.JS;
        }

        public bool Enabled { get; set; }

        public ErrorCategory ErrorCategory { get; set; }

        public bool TODOEnabled {
			get { return JSLintOptions.FindTodos; }
			set { JSLintOptions.FindTodos = value; }
		}

        public ErrorCategory TODOCategory { get; set; }

        public bool RunOnBuild { get; set; }

        public bool CancelBuildOnError { get; set; }

        public JSLintOptions JSLintOptions { get; set; }

		public IncludeFileType BuildFileTypes { get; set; }

		public IncludeFileType SaveFileTypes { get; set; }

		public bool FakeCSSCharset { get; set; }

		public string IgnoreErrorStart { get; set; }

		public string IgnoreErrorEnd { get; set; }

		public string IgnoreErrorLine { get; set; }

        public bool RunOnSave { get; set; }

        internal void SaveAs(string filename)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Options));
            using (FileStream fout = File.Create(filename))
            {
                serializer.Serialize(fout, this);
            }
        }

        internal void SaveChanges()
        {
            lock (this)
            {
                SaveAs(OptionsPath);
            }
        }

        internal static Options Load(string filename)
        {
            try
            {
                if (File.Exists(filename))
                {
                    using (FileStream fin = File.OpenRead(filename))
                    {
                        return Load(fin);
                    }
                }
            }
            catch
            {
                // called in a static context so we should never throw an exception. Return default options instead
            }
            return new Options();
        }

        internal static Options Load(Stream fin)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Options));
            return (Options)serializer.Deserialize(fin);
        }

        internal static Options ReloadCurrent()
        {
            return _current = Load(OptionsPath);
        }
    }
}

