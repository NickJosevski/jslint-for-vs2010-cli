﻿
namespace JSLint.VS2010.OptionClasses
{
    using System.IO;
    using System.Text;
    using System.Collections.Generic;
    using System;
    using System.Linq;
    using System.Xml.Serialization;

    [Flags]
    public enum Linters
    {
        JSLint = 1,
        JSLintOld = 2,
        JSHint = 4,
        All = int.MaxValue
    }

	public sealed class JSLintOptions
    {
        public JSLintOptions()
        {
            SelectedLinter = Linters.JSLint; //not just for preference for backwards compat too
        }

        public JSLintBoolOption BoolOptions 
        {
            set
            {
				if (value == JSLintBoolOption.OptionsUpgraded)
				{
					return;
				}

                BoolOptions2 = new SerializableDictionary<string, bool>();
                foreach (JSLintBoolOption option in Enum.GetValues(typeof(JSLintBoolOption)))
                {
                    if (option == JSLintBoolOption.OptionsUpgraded)
                    {
                        continue;
                    }
                    string optionName = option.ToString();
                    if (option == JSLintBoolOption.tolcontinue)
                    {
                        optionName = "continue";
                    }
                    BoolOptions2.Add(optionName, (option & value) == option);
                }
            }
            get
            {
                return JSLintBoolOption.OptionsUpgraded;
            }
        }

        public SerializableDictionary<string, bool> BoolOptions2 { get; set; }

        //TODO - remove
        public bool UseOldVersion 
		{
			get
			{
				return false;
			}
			set
			{
				if (value)
				{
					SelectedLinter = Linters.JSLintOld;
				}
			}
		}

        public bool ErrorOnUnused { get; set; }

        public bool ErrorOnRepeatedConstant { get; set; }

        public int? IndentSize { get; set; }

        public int? MaxLength { get; set; }

		public Linters SelectedLinter { get; set; }

		public bool FindTodos { get; set; }

        /// <summary>
        ///  List of strings predefined
        /// </summary>
        public List<string> PreDefined { get; set; }

        /// <summary>
        ///  Creates an (javascript compatible) object that JsLint can use for options.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> ToJsOptionVar(Linters linterToUse)
        {
            Dictionary<string, object> returner = new Dictionary<string, object>();
			
            foreach (KeyValuePair<string, bool> option in BoolOptions2)
            {
                if (LintBooleanSettingModel.AppliesTo(option.Key, linterToUse))
                {
                    returner[option.Key.TrimEnd('.')] = option.Value;
                }
            }

            if (PreDefined != null && PreDefined.Count > 0)
            {
                returner["predef"] = PreDefined.ToArray();
            }

            if (MaxLength.HasValue)
            {
                returner["maxlen"] = MaxLength.Value;
            }

            if (IndentSize.HasValue)
            {
                returner["indent"] = IndentSize.Value;
            }

            returner["maxerr"] = 499; //we stop at 499 errors in connect so we may as well let lint stop and generate a 499th error telling us where it got to.

            return returner;
        }

        internal static JSLintOptions Default
        {
            get
            {
                return new JSLintOptions
                {
                    BoolOptions2 = LintBooleanSettingModel.GetDefaultOptions(), 
                    IndentSize = 4,
                    MaxLength = 90,
                    ErrorOnUnused = true,
                };
            }
        }
    }
}

