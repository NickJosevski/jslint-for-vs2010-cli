﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using JSLint.VS2010.LinterBridge;
using JSLint.VS2010.OptionClasses;

namespace JSLint.CLI
{
    /// <summary>
    /// Takes a file and processes it for JSLint errors
    /// </summary>
    public class JsLintCommandLine
    {
        /// <summary>
        /// Main method, takes args of directory containing files
        /// Makes use of 'JSLintOptions.xml' which must be deployed with the command line util
        /// </summary>
        /// <param name="args"> folder path </param>
        /// <returns>error code
        /// Error levels:
        ///         0 - Success
        ///         1 - JavaScript warnings
        ///         2 - Usage or configuration error
        ///         3 - JavaScript error
        ///         4 - File error
        /// </returns>
        public static int Main(string[] args)
        {
            var returnCode = 0;
            if (args == null || args.Length != 1)
            {
                Console.WriteLine("unexpected args, must only supply folder path");
                returnCode = 3;
                return returnCode;
            }

            try
            {
                var linter = new JSLinter();
                var text = String.Empty;
                var errorCount = 0;

                Console.WriteLine("JsLintCommandLine path supplied is '{0}'", args[0]);

                var javaScriptFiles = Directory.GetFiles(args[0], "*.js");

                Console.WriteLine("JsLintCommandLine is starting and will process '{0}' files in total", javaScriptFiles.Length);

                foreach (var file in javaScriptFiles)
                {
                    using (var reader = new StreamReader(file))
                    {
                        text = reader.ReadToEnd();
                    }

                    var result = linter.Lint(text, Options.Current.JSLintOptions, true);

                    foreach (var error in result)
                    {
                        Console.WriteLine(error.ToString());
                        errorCount++;
                    }
                }

                if (errorCount > 0)
                {
                    Console.WriteLine("JsLintCommandLine complete there were '{0}' errors in total", errorCount);
                    returnCode = 3;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in the JSLint CLI processor{0}{1}", Environment.NewLine, ex.Message);
                returnCode = 4;
            }

            // all was fine
            return returnCode;
        }
    }
}
