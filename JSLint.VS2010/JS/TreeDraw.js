﻿var treeDraw = function (what, context, depth, iam) {
    var i, v, j = 0, k = 0, siam = String(iam), mytree = '';
    mytree += String(iam);
    for (i = 0; i < depth - 1; i++) {
        mytree += '  ';
    }
    for (i = siam.length; i < 7; i++) {
        mytree += ' ';
    }
    for (i in what) {
        if (what.hasOwnProperty(i)) {
            v = what[i];
            //if  (typeof v === 'string' || typeof v === 'number') {

            if (j++ !== 0) {
                mytree += ', ';
            }
            if (i === 'prev' || i === 'next') {
                mytree += i + '=[' + v.value + '-' + v.arity + ']';
            } else {
                if (i === 'postcomments' || i === 'comments') {
                    mytree += i + ' = ';
                    for (k = 0; k < v.length; k++) {
                        mytree += v[k].comment + '][';
                    }
                } else {
                    mytree += i + '=' + v;
                    if (typeof v !== 'string' && typeof v !== 'number') {
                        mytree += '(' + typeof v + ')';
                    }
                }
            }
        }
    }
    mytree += '\n';
    context.tree = (context.tree || '') + mytree;
};