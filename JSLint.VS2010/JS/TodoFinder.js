﻿var todoAllowedWhitespace = " \t\r\n";
var todoFinder = function (what, context, depth, iam) {
    var comments, commentLines, i, j, comment, index, loc;
    context.errs = context.errs || [];
    comments = (what.comments || []).concat(what.postcomments || []);
    for (i = 0; i < comments.length; i++) {
        commentLines = comments[i].comment;
        for (j = 0; j < commentLines.length; j++) {
            comment = commentLines[j].toLowerCase();
            index = 0;
            while (true) {
                index = comment.indexOf('todo', index);
                if (index === 0 || (index > 0 && (todoAllowedWhitespace.indexOf(comment.charAt(index - 1)) >= 0))) {
                    context.errs.push({ reason: commentLines[j].substring(index), line: comments[i].line });
                    break;
                }
                if (index < 0) {
                    break;
                }
                index++;
            }
        }
    }
};