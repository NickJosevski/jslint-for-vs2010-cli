﻿var vsdocChecker = (function () {

    var report = function (what, context, reason, lineOffset) {
        context.errs.push({ line: what.line + (lineOffset || 0), reason: reason });
    },
        vsdocExtracter = function (what, context, comments) {
            if (!comments) {
                return comments;
            }
            var i, commentLine, interrupted = false, vsdoc = '';
            for (i = 0; i < comments.length; i++) {
                commentLine = comments[i];
                if (commentLine.indexOf('/') === 0) {
                    if (interrupted) {
                        report(what, context, 'vsdoc comment interrupted with normal comment', i - 1);
                    } else {
                        vsdoc += commentLine.substring(1);
                    }
                } else {
                    interrupted = true;
                }
            }
            return vsdoc;
        };

    return function (what, context, depth, iam) {
        var comments;
        context.errs = context.errs || [];
        context.opt = context.opt || { allfuncshavesummary: true };
        if (depth === 1 || (depth === 2 && iam === '0')) {
            comments = vsdocExtracter(what, context, what.comments);
            if (comments) {
                //parse reference
            }
        } else if (what.arity === 'function') {
            comments = null;
            if (what.block && what.block['0']) {
                comments = what.block['0'].comments;
            }

            comments = vsdocExtracter(what, context, comments);

            if (!comments && context.opt.allfuncshavesummary) {
                report(what, context, 'missing vsdoc comment');
            } else {
                // parse summary, field, value
            }
        } else {
            comments = vsdocExtracter(what, context, what.comments);
            if (comments) {
                report(what, context, 'invalid place for vsdoc - should be immediately ' +
                    'after function { or at begining of file if references');
            }
        }
    };
}());