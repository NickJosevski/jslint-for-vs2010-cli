﻿/*globals extractRepeatingVars, processRepeatingVars, vsdocChecker, treeDraw*/
var traverseRe = /^(first|second|third|else|block|\d+)$/,
    traverseInstanceCount = 1;

var traverse = function (data, func, context, depth, iam, traverseInstance) {
    var k, v;

    depth = depth || 1;
    iam = iam || '';
    context = context || {};
    //traverseInstance = traverseInstance || traverseInstanceCount++;

    func(data, context, depth, iam);

    for (k in data) {
        if (data.hasOwnProperty(k)) {
            v = data[k];
            if (v && traverseRe.test(k)) { // && v.visited !== traverseInstance) {
                //v.visited = traverseInstance;
                if (typeof v === 'object') {
                    context = traverse(v, func, context, depth + 1, k); //, traverseInstance);
                }
            }
        }
    }

    return context;
};

var getGlobalObject = function() {
    return (function(){
        return this;
    }).call(null);
}

var lintRunner = function (linterName, dataCollector, javascript, options, supportsTree, extensions) {
    var success,
		data,
		strs,
        report = {},
        linterFunction = getGlobalObject()[linterName];

    success = linterFunction(javascript, options);
    data = linterFunction.data();

    if (data) {
        report.errors = data.errors;
        report.unused = data.unused;
        // only passing errors and unused because of bug in Noesis Javascript.NET
        // which errors on { '1' : true } - objects with integer fields
    }

    if (success && supportsTree) {
        try {
            //if (extensions.runTree) {
            //    report.tree = traverse(linterFunction.tree, treeDraw).tree;
            //}

            if (extensions.extractConstants) {
                strs = traverse(linterFunction.tree, extractRepeatingVars);
                report.constants = processRepeatingVars(strs);
            }

            //if (extensions.todoFinder) {
            //    report.todos = traverse(linterFunction.tree, todoFinder).errs;
            //}

            //if (extensions.runVsDoc) {
            //    report.vsdocerr = traverse(linterFunction.tree, vsdocChecker).errs;
            //}
        } catch (e) {
            report.processingErrors = 'error :' + e.stack;
        }
    }

    dataCollector.ProcessData(report);
};