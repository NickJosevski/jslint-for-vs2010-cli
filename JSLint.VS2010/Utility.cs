using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using EnvDTE;

namespace JSLint.VS2010
{
    internal static class Utility
    {
        internal static readonly string AppDataDir =
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

        public static readonly Encoding Encoding = Encoding.GetEncoding("iso-8859-1");

        public static string GetFilename(string name)
        {
            return Path.Combine(AppDataDir, name);
        }

        private static readonly string WebsiteCacheDir = string.Concat(
            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
            "\\Microsoft\\WebsiteCache\\");

        private static readonly string WebsitesFileName = string.Concat(
            WebsiteCacheDir, "Websites.xml");

        public static string GetWebsiteCacheFolder(Project proj)
        {
            if (File.Exists(WebsitesFileName))
            {
                XDocument doc = XDocument.Load(WebsitesFileName);
                XAttribute attr = (from e in doc.Root.Elements("Website")
                                   where e.Attribute("RootUrl").Value.Equals(
                                    proj.FullName, StringComparison.OrdinalIgnoreCase)
                                   select e.Attribute("CacheFolder")).FirstOrDefault();
                if (attr != null)
                {
                    return string.Concat(WebsiteCacheDir, attr.Value, "\\");
                }
            }

            return null;
        }

        private static readonly Type type = typeof(Utility);

        public static string ReadResourceFile(string name)
        {
            using (StreamReader sr = new StreamReader(
                type.Assembly.GetManifestResourceStream("JSLint.VS2010." + name),
                Encoding))
            {
                return sr.ReadToEnd();
            }
        }
    }
}

