using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle("JSLint for Visual Studio 2010")]
[assembly: AssemblyDescription("Adds JSLint support into Visual Studio")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("JSLint.VS2010")]
[assembly: AssemblyCopyright("Dennis Myren, Luke Page")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(false)]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: InternalsVisibleTo("JSLint.VS2010.test, PublicKey=002400000480000094000000060200000024000052534131000400000100010033a9cd924a28006d34badd08958ae4141da7961dec1abece72a21d47fac18303f106ab1a904057750eb9bb3c3dcc758957038c8649f33c070f2ab242ef630020b9d3895c31310776e1827eaa5b270209d768782c3fa2a3df663bba7b9adc2678c317360496629dad56f444cdf5e9cdfd8a07f0305cdba7eb7c426c9a60b7f48a")]
[assembly: InternalsVisibleTo("JSLint.CLI, PublicKey=002400000480000094000000060200000024000052534131000400000100010033a9cd924a28006d34badd08958ae4141da7961dec1abece72a21d47fac18303f106ab1a904057750eb9bb3c3dcc758957038c8649f33c070f2ab242ef630020b9d3895c31310776e1827eaa5b270209d768782c3fa2a3df663bba7b9adc2678c317360496629dad56f444cdf5e9cdfd8a07f0305cdba7eb7c426c9a60b7f48a")]

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Revision
//      Build Number
//
// You can specify all the value or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("1.3.6")]

//
// In order to sign your assembly you must specify a key to use. Refer to the 
// Microsoft .NET Framework documentation for more information on assembly signing.
//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified - the assembly cannot be signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. 
//   (*) If the key file and a key name attributes are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP - that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the file is installed into the CSP and used.
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
//